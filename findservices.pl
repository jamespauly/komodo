#!/usr/bin/perl -w
use strict;
use warnings;

my %services = ();

open SERVICES,
"< /Users/jamesp/svn/R15.1.0_ESVS_Graphical/src/com/homedepot/mm/sv/esvs/delegates/Services.mxml"
  or die $!;

#open SERVICES, "< /Volumes/Macintosh HD 2/svn/R15.1.0_ESVS_Graphical/src/com/homedepot/mm/sv/esvs/delegates/Services.mxml" or die $!;

while (<SERVICES>) {
    if ( $_ =~ m/\s+public static var (\w+)\s*=\s*"([0-9a-zA-Z\/#:]+)"/ ) {
        $services{$1} = $2;

        #print "The Special One: $1 - $2\n";
    }
    if ( $_ =~ m/\s+public static var (\w+):\w+\s*=\s*"([0-9a-zA-Z\/#:]+)"/ ) {

        #/public static var (\w+):\w+\s*=\s*"([0-9a-zA-Z\/#:]+)"/;
        $services{$1} = $2;

        #print "$1 - $2\n";
    }
    if ( $_ =~ m/\s+static public var (\w+)\s*=\s*"([0-9a-zA-Z\/#:]+)"/ ) {
        $services{$1} = $2;

        #print "The Special One: $1 - $2\n";
    }
    if ( $_ =~ m/\s+static public var (\w+):\w+\s*=\s*"([0-9a-zA-Z\/#:]+)"/ ) {

        #/public static var (\w+):\w+\s*=\s*"([0-9a-zA-Z\/#:]+)"/;
        $services{$1} = $2;

        #print "$1 - $2\n";
    }
    if ( $_ =~ m/<mx:HTTPService id="(\w+)"/ ) {
        my $serviceName = $1;
        my $nextline    = <SERVICES>;
        $nextline =~ /url="([A-Za-z0-9_{}:\/.]+)"/;
        my $serviceUrl = $1;
        $services{$serviceName} = $serviceUrl;

    }

}
close(SERVICES);

my $dir = '/Users/jamesp/svn/R15.1.0_ESVS_Graphical/src/com/homedepot/mm/sv/esvs/delegates';

    opendir(DIR, $dir) or die $!;

    while (my $file = readdir(DIR)) {

        # We only want files
        next unless (-f "$dir/$file");

        # Use a regular expression to find files ending in .txt
        next unless ($file =~ m/\.as$/);

        print "$file\n";
        BuildServices(\%services, $file);
    }

    closedir(DIR);

BuildServices(\%services);

sub COSServices {

    open COSDELEGATE,
"< /Users/jamesp/svn/R15.1.0_ESVS_Graphical/src/com/homedepot/mm/sv/esvs/delegates/COSDelegate.as"
      or die $!;

#open OUSDELEGATE, "< /Volumes/Macintosh HD 2/svn/R15.1.0_ESVS_Graphical/src/com/homedepot/mm/sv/esvs/delegates/OUSDelegate.as" or die $!;

    my $storeuri = $services{"STORE_URI"};
    my $domain   = $services{"DEFAULT_DOMAIN"};
    my $wservice = $services{"COS_SERVICE"};

    my $fuctionName;
    my $method;
    my $urlparams;
    my $urlbuilder;
    my $httpmethod = "";
    my $defaulturl =
      $storeuri . "9999" . $domain . $wservice . "{cosMethod}{cosParameters}";
    my $url = "";

    my %urlbuilder = ();

    while (<COSDELEGATE>) {
        if ( $_ =~ m/public function (\w+)\(/ ) {
            if ( $url eq "" ) {

                #code
            }
            else {
                $url =~ s/{cosParameters}//g;
                if ( $httpmethod eq "" ) {
                    $urlbuilder{"url_$fuctionName"} = $url;
                }
                else {
                    $urlbuilder{"url_$fuctionName"} = "$httpmethod - $url";
                }

                $urlbuilder{"url_$fuctionName"} = "$httpmethod - $url";
                $httpmethod = "";
            }
            $fuctionName = $1;
            $url         = $defaulturl;
        }
        if ( $_ =~ m/services.cosMethod\s*=\s*Services\.(.*);/ ) {
            $method = $1;
            $url =~ s/{cosMethod}/$services{$method}/g;
        }
        if ( $_ =~ m/cosService.method\s*=\s*SVSModelLocator\.(.*);/ ) {
            $httpmethod = $1;
        }
        if ( $_ =~ m/services.cosParameters=(.*);/ ) {
            $urlparams = $1;
            $url =~ s/{cosParameters}/$urlparams/g;
        }
    }
    close(COSDELEGATE);

    open( OUT, ">/Users/jamesp/Desktop/cosendpoints.txt" );

    foreach my $key ( sort keys %urlbuilder ) {
        print OUT $key . "," . $urlbuilder{$key} . "\n";
    }
    close(OUT);
}

sub OUSServices {

    open COSDELEGATE,
"< /Users/jamesp/svn/R15.1.0_ESVS_Graphical/src/com/homedepot/mm/sv/esvs/delegates/COSDelegate.as"
      or die $!;

#open OUSDELEGATE, "< /Volumes/Macintosh HD 2/svn/R15.1.0_ESVS_Graphical/src/com/homedepot/mm/sv/esvs/delegates/OUSDelegate.as" or die $!;

    my $storeuri = $services{"STORE_URI"};
    my $domain   = $services{"DEFAULT_DOMAIN"};
    my $wservice = $services{"OUS_SERVICE"};

    my $fuctionName;
    my $method;
    my $urlparams;
    my $urlbuilder;
    my $httpmethod = "";
    my $defaulturl =
      $storeuri . "9999" . $domain . $wservice . "{ousMethod}{ousParameters}";
    my $url = "";

    my %urlbuilder = ();

    while (<OUSDELEGATE>) {
        if ( $_ =~ m/public function (\w+)\(/ ) {
            if ( $url eq "" ) {

                #code
            }
            else {
                $url =~ s/{ousParameters}//g;
                if ( $httpmethod eq "" ) {
                    $urlbuilder{"url_$fuctionName"} = $url;
                }
                else {
                    $urlbuilder{"url_$fuctionName"} = "$httpmethod - $url";
                }

                $urlbuilder{"url_$fuctionName"} = "$httpmethod - $url";
                $httpmethod = "";
            }
            $fuctionName = $1;
            $url         = $defaulturl;
        }
        if ( $_ =~ m/services.ousMethod\s*=\s*Services\.(.*);/ ) {
            $method = $1;
            $url =~ s/{ousMethod}/$services{$method}/g;
        }
        if ( $_ =~ m/ousService.method\s*=\s*SVSModelLocator\.(.*);/ ) {
            $httpmethod = $1;
        }

        #if ($_ =~ m/services.ousParameters=([0-9A-Za-z.+& "?=]*);/) {
        if ( $_ =~ m/services.ousParameters=(.*);/ ) {
            $urlparams = $1;
            $url =~ s/{ousParameters}/$urlparams/g;
        }
    }
    close(OUSDELEGATE);

    open( OUT, ">/Users/jamesp/Desktop/ousendpoints.txt" );

    foreach my $key ( sort keys %urlbuilder ) {
        print OUT $key . "," . $urlbuilder{$key} . "\n";
    }
    close(OUT);
}

sub SLSServices {
    my $params = shift;
    my %services = %$params;

    open SLSDELEGATE,
"< /Users/jamesp/svn/R15.1.0_ESVS_Graphical/src/com/homedepot/mm/sv/esvs/delegates/SLSDelegate.as"
      or die $!;

#open OUSDELEGATE, "< /Volumes/Macintosh HD 2/svn/R15.1.0_ESVS_Graphical/src/com/homedepot/mm/sv/esvs/delegates/OUSDelegate.as" or die $!;

    my $storeuri = $services{"STORE_URI"};
    my $domain   = $services{"DEFAULT_DOMAIN"};
    my $wservice = $services{"SLS_SERVICE"};

    my $fuctionName;
    my $method;
    my $urlparams;
    my $urlbuilder;
    my $httpmethod = "";
    my $defaulturl =
      $storeuri . "9999" . $domain . $wservice . "{slsMethod}{slsParameters}";
    my $url = "";

    my %urlbuilder = ();

    while (<SLSDELEGATE>) {
        if ( $_ =~ m/public function (\w+)\(/ ) {
            if ( $url eq "" ) {

                #code
            }
            else {
                $url =~ s/{slsParameters}//g;
                if ( $httpmethod eq "" ) {
                    $urlbuilder{"url_$fuctionName"} = $url;
                }
                else {
                    $urlbuilder{"url_$fuctionName"} = "$httpmethod - $url";
                }

                $urlbuilder{"url_$fuctionName"} = "$httpmethod - $url";
                $httpmethod = "";
            }
            $fuctionName = $1;
            $url         = $defaulturl;
        }
        if ( $_ =~ m/services.Method\s*=\s*Services\.(.*);/ ) {
            $method = $1;
            $url =~ s/{slsMethod}/$services{$method}/g;
        }
        if ( $_ =~ m/slsService.method\s*=\s*SVSModelLocator\.(.*);/ ) {
            $httpmethod = $1;
        }

        #if ($_ =~ m/services.slsParameters=([0-9A-Za-z.+& "?=]*);/) {
        if ( $_ =~ m/services.slsParameters=(.*);/ ) {
            $urlparams = $1;
            $url =~ s/{slsParameters}/$urlparams/g;
        }
    }
    close(SLSDELEGATE);

    open( OUT, ">/Users/jamesp/Desktop/slsendpoints.txt" );

    foreach my $key ( sort keys %urlbuilder ) {
        print OUT $key . "," . $urlbuilder{$key} . "\n";
    }
    close(OUT);

}

sub BuildServices {
    my ($services, $file) = @_;
    
    #my $params = shift;
    #my %services = %$params;
    
    my $apiName = 'SLS';
    my $lcApiName = lc($apiName);
    my $ucApiName = uc($apiName);
    
    my $delegateFile = "/Users/jamesp/svn/R15.1.0_ESVS_Graphical/src/com/homedepot/mm/sv/esvs/delegates/".$ucApiName."Delegate.as";

    open DELEGATE,"< $file"
      or die $!;

#open DELEGATE, "< /Volumes/Macintosh HD 2/svn/R15.1.0_ESVS_Graphical/src/com/homedepot/mm/sv/esvs/delegates/OUSDelegate.as" or die $!;

    my $storeuri = $services{"STORE_URI"};
    my $domain   = $services{"DEFAULT_DOMAIN"};
    my $wservice = $services{$ucApiName."_SERVICE"};

    my $fuctionName;
    my $regex;
    my $method;
    my $urlparams;
    my $urlbuilder;
    my $httpmethod = "";
    my $defaulturl =
      $storeuri . "9999" . $domain . $wservice . "{".$lcApiName."Method}{".$lcApiName."Parameters}";
    my $url = "";

    my %urlbuilder = ();

    while (<DELEGATE>) {
        if ( $_ =~ m/public function (\w+)\(/ ) {
            if ( $url eq "" ) {

                #code
            }
            else {
                $regex = '{\w+Parameters}';
                $url =~ s/$regex//g;
                if ( $httpmethod eq "" ) {
                    $urlbuilder{"url_$fuctionName"} = $url;
                }
                else {
                    $urlbuilder{"url_$fuctionName"} = "$httpmethod - $url";
                }

                $urlbuilder{"url_$fuctionName"} = "$httpmethod - $url";
                $httpmethod = "";
            }
            $fuctionName = $1;
            $url         = $defaulturl;
        }
        $regex = 'services.\w+Method\w*\s*=\s*Services\.(.*);';
        if ( $_ =~ m/$regex/ ) {
            $method = $1;
            $regex = '{\w+Method}';
            #$url =~ s/{slsMethod}/$services{$method}/g;
            $url =~ s/$regex/$services{$method}/g;
            
        }
        $regex = '\w+Service.method\s*=\s*SVSModelLocator\.(.*);';
        if ( $_ =~ m/$regex/ ) {
            $httpmethod = $1;
        }
        $regex = 'services.\w+Parameters=(.*);';
        if ( $_ =~ m/$regex/ ) {
            $urlparams = $1;
            $regex = '{\w+Parameters}';
            $url =~ s/$regex/$urlparams/g;
        }
    }
    close(DELEGATE);

    open( OUT, ">/Users/jamesp/Desktop/".$lcApiName."endpoints.txt" );

    foreach my $key ( sort keys %urlbuilder ) {
        print OUT $key . "," . $urlbuilder{$key} . "\n";
    }
    close(OUT);

}

#public function getActionOptionDetails(custOrdNbr:String, svcLineNbr:String, svcTypeCd:String, svcStatCd:String):void
#		{
#			services.ousMethod=Services.OUS_ACTION_OPTION_DETAILS_GET;
#			services.ousParameters="?custOrdNbr=" + custOrdNbr + "&svcLineNbr=" + svcLineNbr + "&svcTypeCd=" + svcTypeCd + "&svcStatCd=" + svcStatCd+ "&userID=" + model.userProfileVO.userId + "&noCache=" + timeStamp;
#			ousService.method=SVSModelLocator.GET;
#			ousService.contentType="";
#
#			var token:AsyncToken=ousService.send();
#			token.addResponder(responder);
#		}

#url="{STORE_URI}{storeNumber}{DEFAULT_DOMAIN}{OUS_SERVICE}{ousMethod}{ousParameters}"

