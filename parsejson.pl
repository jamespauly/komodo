#!/usr/bin/perl -w
use strict;

use JSON;
use Data::Dumper;
use REST::Client;

$ENV{'PERL_LWP_SSL_VERIFY_HOSTNAME'} = 0;

my $num_args = $#ARGV + 1;
if ( $num_args < 2 ) {
    print "\nUsage: myops.pl method site user env author(s)\n";
    print
      "\nMethods: getauthors, addauthors, addprimaryauthor, deleteauthors\n";
    exit;
}

my $method = $ARGV[0];
my $site   = $ARGV[1];
my $user   = $ARGV[2];
my $env    = $ARGV[3];

my @nAuthors = splice @ARGV, 4;

my $host;
if ( $env eq 'qa' ) {
    $host = 'https://webapps-qa.homedepot.com';
}
elsif ( $env eq 'prod' ) {
    $host = 'https://webapps.homedepot.com';
}

my @authors;

if ( $method eq 'getauthors' ) {
    getAuthors( $host, $site, $user );
}
elsif ( $method eq 'addauthors' ) {
    addAuthors( $host, $site, $user, \@nAuthors );
}
elsif ( $method eq 'addprimaryauthor' ) {
    addPrimaryAuthor( $host, $site, $user, \@nAuthors );
}
elsif ( $method eq 'deleteauthors' ) {
    deleteAuthors( $host, $site, $user, \@nAuthors );
}

sub addPrimaryAuthor {
    my ( $host, $siteId, $user, $nAuthors ) = @_;

    if ( scalar @{$nAuthors} > 1 ) {
        print "\nYou can only add one primary Author!\n";
        exit;
    }

    my @authors = getAuthors( $host, $siteId );

    prependAuthors( \@authors, $nAuthors );

    putAuthors( $host, $siteId, $user, \@authors );
}

sub addAuthors {
    my ( $host, $siteId, $user, $nAuthors ) = @_;

    my @authors = getAuthors( $host, $siteId );

    buildAuthors( \@authors, $nAuthors );

    putAuthors( $host, $siteId, $user, \@authors );
}

sub deleteAuthors {
    my ( $host, $siteId, $user, $nAuthors ) = @_;

    my @authors = getAuthors( $host, $siteId );

    removeAuthors( \@authors, $nAuthors );

    putAuthors( $host, $siteId, $user, \@authors );
}

sub prependAuthors {
    my ( $authors, $nAuthors ) = @_;

    foreach my $sAuthor ( @{$nAuthors} ) {

        my $nauthor = {
            'firstName'    => undef,
            'lastName'     => undef,
            'ldapId'       => "$sAuthor",
            'authorId'     => undef,
            'orgInfo'      => undef,
            'emailAddress' => undef,
            'middleName'   => undef
        };

        unshift @{$authors}, $nauthor;
    }
}

sub buildAuthors {
    my ( $authors, $nAuthors ) = @_;

    foreach my $sAuthor ( @{$nAuthors} ) {

        my $nauthor = {
            'firstName'    => undef,
            'lastName'     => undef,
            'ldapId'       => "$sAuthor",
            'authorId'     => undef,
            'orgInfo'      => undef,
            'emailAddress' => undef,
            'middleName'   => undef
        };

        push @{$authors}, $nauthor;
    }
}

sub removeAuthors {
    my ( $authors, $nAuthors ) = @_;

    foreach my $sAuthor (@nAuthors) {
        my $index = 0;
        my $size  = scalar $authors;
        foreach my $author ( @{$authors} ) {
            print "testing $author->{ldapId}\n";
            if ( lc( $author->{ldapId} ) eq lc($sAuthor) ) {
                print "Removed user $author->{ldapId}\n";
                splice $authors, $index, 1;
            }
        }
    }
}

sub putAuthors {
    my ( $host, $siteId, $user, $authors ) = @_;

    my $client = REST::Client->new();
    $client->setHost($host);

    $client->PUT(
        "/thdMyOps/rest/sites/$siteId/authors",
        encode_json($authors),
        {
            'Content-Type' => 'application/json',
            'Accept'       => 'application/json',
            'ldapId'       => "$user"
        }
    );

    my $responseJson = $client->responseContent();
    print 'Status: ' . $client->responseCode() . "\n";
    print 'Response: ' . $responseJson . "\n\n";
    
    my $decoded = decode_json($responseJson);
    my @authors = @{$decoded};
    
     my @alist;

    foreach my $author (@authors) {
        #print $author->{"ldapId"} . "\n";
        push @alist, $author->{"ldapId"};
    }
    
    print "Current Authors in Site Id $siteId: ".join( ',', @alist )."\n\n";
    
        print "------ PUT Transaction Done ------\n\n";
}

sub getAuthors {
    my ( $host, $siteId ) = @_;

    my $client = REST::Client->new();
    $client->setHost($host);
    $client->GET("/thdMyOps/rest/sites/$siteId/authors");

    my $responseJson = $client->responseContent();
    print 'Status: ' . $client->responseCode() . "\n";
    print 'Response: ' . $responseJson . "\n\n";

    my $decoded = decode_json($responseJson);
    my @authors = @{$decoded};
 
    my @alist;

    foreach my $author (@authors) {
        #print $author->{"ldapId"} . "\n";
        push @alist, $author->{"ldapId"};
    }
    
    print "Current Authors in Site Id $siteId: ".join( ',', @alist )."\n\n";
    
    print "------ GET Transaction Done ------\n\n";
    
    return @authors;
}

